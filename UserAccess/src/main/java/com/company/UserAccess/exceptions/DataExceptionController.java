package com.company.UserAccess.exceptions;

import com.company.UserAccess.model.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ControllerAdvice
public class DataExceptionController {

    @ExceptionHandler(value = DataNotFoundException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ExceptionResponse exception(DataNotFoundException exception) {
        return new ExceptionResponse(LocalDateTime.now(), exception.getMessage());
    }

}
