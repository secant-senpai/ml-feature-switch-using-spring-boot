package com.company.UserAccess.exceptions;

import com.company.UserAccess.model.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ControllerAdvice
public class ModificationExceptionController {

    @ExceptionHandler(value = DataNotModifiedException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_MODIFIED)
    public ExceptionResponse exception(DataNotModifiedException exception) {
        return new ExceptionResponse(LocalDateTime.now(), exception.getMessage());
    }

}