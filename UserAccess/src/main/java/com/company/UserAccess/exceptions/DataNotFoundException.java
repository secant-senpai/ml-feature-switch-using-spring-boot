package com.company.UserAccess.exceptions;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException() {
        super("User or feature not found in database.");
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
