package com.company.UserAccess.exceptions;

public class DataNotModifiedException extends RuntimeException {

    public DataNotModifiedException() {
        super("Similar access level detected, database not modified.");
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
