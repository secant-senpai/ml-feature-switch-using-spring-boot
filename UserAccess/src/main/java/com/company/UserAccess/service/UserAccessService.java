package com.company.UserAccess.service;

import com.company.UserAccess.exceptions.DataNotFoundException;
import com.company.UserAccess.exceptions.DataNotModifiedException;
import com.company.UserAccess.model.Feature;
import com.company.UserAccess.model.User;
import com.company.UserAccess.model.UserFeatureWrapper;
import com.company.UserAccess.repository.FeatureRepository;
import com.company.UserAccess.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public record UserAccessService(UserRepository userRepository, FeatureRepository featureRepository) {

    public Optional<User> searchForUser(String email) {
        return userRepository.findByEmail(email);
    }

    public Optional<Feature> searchForFeature(String featureName) {
        return featureRepository.findByFeatureName(featureName);
    }

    public boolean getUserAccessLevel(String email, String featureName) throws DataNotFoundException {

        Optional<User> searchedUser = searchForUser(email);
        Optional<Feature> searchedFeature = searchForFeature(featureName);

        if (searchedUser.isEmpty() || searchedFeature.isEmpty()) {
            log.info("User or feature not found in database.");
            throw new DataNotFoundException();
        }

        User userToGet = searchedUser.get();
        Feature featureToGet = searchedFeature.get();

        log.info("Getting access level...");
        return userToGet.getEnabledFeatures().contains(featureToGet);

    }

    public void updateUserFeatureAccess(UserFeatureWrapper input) throws DataNotModifiedException {

        Optional<User> searchedUser = searchForUser(input.getEmail());
        Optional<Feature> searchedFeature = searchForFeature(input.getFeatureName());

        if (searchedUser.isEmpty() || searchedFeature.isEmpty()) {
            // if requested user or feature does not exist in database, reject change request.
            log.info("User or feature not found in database.");
            throw new DataNotModifiedException();
//            throw new DataNotFoundException();            // Should be using this, but I have to conform to the specs.
        }

        int userAccessLevel = getUserAccessLevel(input.getEmail(), input.getFeatureName()) ? 1 : 0;
        if (userAccessLevel == input.isCanAccess()) {
            // if requested user already has the same feature access level, reject change request.
            log.info("Similar access level detected, abort process.");
            throw new DataNotModifiedException();
        }

        // else, adds or remove feature access for the user as desired.
        log.info("Updating database...");

        User userToUpdate = searchedUser.get();
        Feature featureToUpdate = searchedFeature.get();

        if (input.isCanAccess() == 0) {
            log.info("Removing user access.");
            userToUpdate.getEnabledFeatures().remove(featureToUpdate);
        } else {
            log.info("Adding user access.");
            userToUpdate.getEnabledFeatures().add(featureToUpdate);
        }
        userRepository.save(userToUpdate);

    }
}
