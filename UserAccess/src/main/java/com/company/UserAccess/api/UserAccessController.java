package com.company.UserAccess.api;

import com.company.UserAccess.exceptions.DataNotFoundException;
import com.company.UserAccess.exceptions.DataNotModifiedException;
import com.company.UserAccess.model.UserFeatureWrapper;
import com.company.UserAccess.service.UserAccessService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@Slf4j
@RequestMapping("/")
@RestController
public record UserAccessController(UserAccessService userAccessService) {

    /**
     * Queries the database for feature access for specified user.
     * @param email User's email
     * @param featureName Queried feature name
     * @return in JSON format, canAccess: (true|false)
     * @throws DataNotFoundException if user/feature not found
     *
     */
    @GetMapping(value = "/feature", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getUserAccessLevel(@RequestParam("email") String email,
                                                  @RequestParam("featureName") @NonNull String featureName) throws DataNotFoundException {

        log.info("Query for user " + email + " and feature " + featureName + ":");
        boolean status = userAccessService.getUserAccessLevel(email, featureName);
        log.info("Fetched access level with status " + status);

        if (status) {
            return Collections.singletonMap("canAccess", "true");
        } else return Collections.singletonMap("canAccess", "false");

    }

    /**
     * Updates the specified user's access level to the requested feature in the database.
     * @param input in JSON format, queried feature name, user's email and enable status (true/false)
     * @return OK if feature access level is modified
     * @throws DataNotFoundException if user/feature not found
     * @throws DataNotModifiedException if database not modified due to similar feature access
     *
     */
    @PostMapping(path = "/feature")
    public ResponseEntity<String> updateUserFeatureAccess(@RequestBody UserFeatureWrapper input) throws DataNotModifiedException {

        log.info("Update feature " + input.getFeatureName() + " for user " + input.getEmail() + ":");
        userAccessService.updateUserFeatureAccess(input);
        log.info("Updated database successfully.");

        return ResponseEntity.ok().body("User access to feature is successfully modified.");

    }
}
