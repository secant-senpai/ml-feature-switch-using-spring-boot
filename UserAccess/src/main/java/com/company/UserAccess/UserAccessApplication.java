package com.company.UserAccess;

import com.company.UserAccess.model.Feature;
import com.company.UserAccess.model.User;
import com.company.UserAccess.repository.FeatureRepository;
import com.company.UserAccess.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@SpringBootApplication
public class UserAccessApplication {

	private static final Logger logger = LoggerFactory.getLogger(UserAccessApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(UserAccessApplication.class, args);
	}

	// Instantiate database with dummy values. Comment out this block to use own data.
	@Bean
	public CommandLineRunner commandLineRunner(UserRepository userRepository, FeatureRepository featureRepository) {
		return args -> {

			// Users
			User user1 = new User(UUID.randomUUID(), "alice@gmail.com");
			User user2 = new User(UUID.randomUUID(), "ahmad@hotmail.com");
			User user3 = new User(UUID.randomUUID(), "lee@yahoo.com");
			User user4 = new User(UUID.randomUUID(), "ron@topglove.com.my");
			logger.info("Added dummy users.");

			// Features
			Set<Feature> exampleFeature = new HashSet<>();
			Feature newUI = new Feature("new_ui_redesign");
			Feature test = new Feature("new_test_feature");
			Feature admin = new Feature("enable_admin_privileges");
			exampleFeature.add(newUI);
			exampleFeature.add(test);
			exampleFeature.add(admin);

			// Add features to database
			featureRepository.saveAll(exampleFeature);
			logger.info("Added dummy features.");

			// Define initial user access (usually for admin accounts)
			user1.getEnabledFeatures().add(newUI);
			user1.getEnabledFeatures().add(test);
			user1.getEnabledFeatures().add(admin);
			userRepository.save(user1);
			user2.getEnabledFeatures().add(newUI);
			user2.getEnabledFeatures().add(test);
			userRepository.save(user2);
			user3.getEnabledFeatures().add(newUI);
			userRepository.save(user3);
			userRepository.save(user4);
			logger.info("Set initial access levels for sample data.");
			logger.info("Ready.");
		};
	}

}
