package com.company.UserAccess.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Id;

public class UserFeatureWrapper {
    private final String featureName;
    private final String email;
    @JsonProperty("enable")
    private final boolean enable;

    public UserFeatureWrapper(String featureName, String email, boolean canAccess) {
        this.featureName = featureName;
        this.email = email;
        this.enable = canAccess;
    }

    public String getFeatureName() {
        return featureName;
    }

    public String getEmail() {
        return email;
    }

    public int isCanAccess() {
        if (enable) {
            return 1;
        }
        else return 0;
    }
}
