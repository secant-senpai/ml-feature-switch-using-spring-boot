package com.company.UserAccess.model;

import javax.persistence.*;

@Entity(name = "Feature")
@Table(
        name = "feature",
        uniqueConstraints = {
                @UniqueConstraint(name = "Feature_name_unique", columnNames = "feature_name")
        }
)
public class Feature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long feature_id;
    @Column(name = "feature_name", nullable = false)
    private String featureName;

    public Feature() {
    }

    public Feature(String featureName) {
        this.featureName = featureName;
    }

    public Feature(Long feature_id, String featureName) {
        this.feature_id = feature_id;
        this.featureName = featureName;
    }

    public Long getFeature_id() {
        return feature_id;
    }

    public String getFeatureName() {
        return featureName;
    }
}
