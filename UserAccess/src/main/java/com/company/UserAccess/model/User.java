package com.company.UserAccess.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity(name = "User")
@Table(
        name = "all_user",
        uniqueConstraints = @UniqueConstraint(name = "user_email_unique", columnNames = "email")
)
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private UUID id;
    @Column(name = "email", nullable = false)
    private String email;

    public User() {
    }

    public User(String email) {
        this.email = email;
    }

    public User(UUID id, String email) {
        this.id = id;
        this.email = email;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_feature",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "feature_id", referencedColumnName = "id")
    )
    private Set<Feature> enabledFeatures = new HashSet<>();

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public Set<Feature> getEnabledFeatures() {
        return enabledFeatures;
    }

    public void setEnabledFeatures(Set<Feature> enabledFeatures) {
        this.enabledFeatures = enabledFeatures;
    }
}
