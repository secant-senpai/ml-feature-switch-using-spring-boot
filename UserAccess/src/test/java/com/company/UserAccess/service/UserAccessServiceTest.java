package com.company.UserAccess.service;

import com.company.UserAccess.model.Feature;
import com.company.UserAccess.model.User;
import com.company.UserAccess.repository.FeatureRepository;
import com.company.UserAccess.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.anyOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserAccessServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private FeatureRepository featureRepository;

    private UserAccessService underTest;

    @BeforeEach
    void setUp() {
        underTest = new UserAccessService(userRepository, featureRepository);
    }

    @Test
    void shouldSearchForUser() {
        UUID dummyId = UUID.randomUUID();
        String dummyEmail = anyString();
        User dummyUser = new User(dummyId, dummyEmail);

        given(userRepository.findByEmail(dummyEmail)).willReturn(Optional.of(dummyUser));
        Optional<User> searchedUserOptional = underTest.searchForUser(dummyEmail);

        verify(userRepository).findByEmail(dummyUser.getEmail());
    }

    @Test
    void shouldSearchForFeature() {
        Long dummyId = 1L;
        String dummyFeatureName = "test";
        Feature dummyFeature = new Feature(dummyId, dummyFeatureName);

        given(featureRepository.findByFeatureName(dummyFeatureName)).willReturn(Optional.of(dummyFeature));
        Optional<Feature> searchedFeatureOptional = underTest.searchForFeature(dummyFeatureName);

        verify(featureRepository).findByFeatureName(dummyFeature.getFeatureName());
    }

    @Test
    void shouldGetUserAccessLevelIfUserAndFeatureBothExist() {
        UUID dummyId = UUID.randomUUID();
        String dummyEmail = anyString();
        User dummyUser = new User(dummyId, dummyEmail);

        Long dummyFeatureId = 1L;
        String dummyFeatureName = "test";
        Feature dummyFeature = new Feature(dummyFeatureId, dummyFeatureName);

        Set<Feature> enabledFeaturesForUser = new HashSet<>();
        enabledFeaturesForUser.add(dummyFeature);
        dummyUser.setEnabledFeatures(enabledFeaturesForUser);

        given(userRepository.findByEmail(dummyEmail)).willReturn(Optional.of(dummyUser));
        Optional<User> searchedUserOptional = userRepository.findByEmail(dummyEmail);

        given(featureRepository.findByFeatureName(dummyFeatureName)).willReturn(Optional.of(dummyFeature));
        Optional<Feature> searchedFeatureOptional = featureRepository.findByFeatureName(dummyFeatureName);

        User searchedUser = searchedUserOptional.get();
        Feature searchedFeature = searchedFeatureOptional.get();

        assertThat(searchedUser).isEqualTo(dummyUser);
        assertThat(searchedFeature).isEqualTo(dummyFeature);

        assertThat(dummyFeature).isIn(searchedUser.getEnabledFeatures());
    }

    @Test
    @Disabled
    void updateUserFeatureAccess() {
    }

    @Test
    @Disabled
    void userRepository() {
    }

    @Test
    @Disabled
    void featureRepository() {
    }
}