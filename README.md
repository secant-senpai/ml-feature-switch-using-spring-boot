# Feature Switch Implementation Using Spring Boot, Spring JPA and PostgreSQL

An API implementing Feature Switching based on Java/Spring Boot created as part of MoneyLion's technical interview.

In this project, **Spring JPA** was used to access an external database instance. This repository also contains a variation which uses **JDBC calls** and can be found in the alternate branch [JDBC_Template](https://gitlab.com/secant-senpai/ml-feature-switch-using-spring-boot/-/tree/JDBC_Template). For more information, refer to [this merge request](https://gitlab.com/secant-senpai/ml-feature-switch-using-spring-boot/-/merge_requests/3).

## Case Study

As Product Manager, one would like to manage users’ accesses to new features via feature switches, i.e. enabling/disabling certain feature based on a user’s email and feature names.

Key takeaways:
1. Determining whether a feature is enabled for a particular user.
2. Providing a mechanism to enable or disable a feature for the specified user.

### Requirements
The API exposes two HTTP endpoints, the GET endpoint and the POST endpoint. The first one queries the database for user access...

```
GET /feature?email=XXX&featureName=XXX
```

Accepting the above request returns the (represented by email)  user's access to the specified featureName. This endpoint outputs the following response in JSON format:

```
{
  "canAccess": true|false
}
```

while the second one updates the database as per desired:

```
POST /feature
```

```
{ 
  "featureName": "XXX",
  "email": "XXX",
  "enable": true|false (uses true to enable a user's access, otherwise)
}
```

Accepting the above request and the JSON input above will update the specified user's access level to the requested feature in the database. The endpoint outputs the following HTTP responses depending on whether the database is updated or not:

| Status | Returned HTTP Status |
|---|---|
| Database not modified:<br>- Requested user or feature does not exist in database.<br>- Requested user already possess the required feature access level. | 304, Not Modified |
| Database modified. | 200, OK |

### Mechanism of JPA Calls
- Create many-to-many relation between the entities User and Feature with User as the owner. The annotation `@JoinTable` joins the two primary keys from all_user and feature tables to form the user_feature table.
```
@ManyToMany(cascade = CascadeType.ALL)
@JoinTable(
        name = "user_feature",
        joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "feature_id", referencedColumnName = "id")
)
private Set<Feature> enabledFeatures = new HashSet<>();
```
- Extend JpaRepository class for each model to enable database CRUD operations.
```
public interface UserRepository extends JpaRepository<User, UUID> {
        // User is the entity, while UUID is the primary key.
}
```
- The database is manipulated through the `Set<Feature>` object.
```
searchedUser.getEnabledFeatures().contains(searchedFeature);    // To query for feature access
userToUpdate.getEnabledFeatures().add(featureToUpdate);         // To add feature access to user
```

### Mock-up Sample Data
Three tables were created at startup, in 'userfeaturedatabase' database as defined in [src/main/resources/application.yml](https://gitlab.com/secant-senpai/ml-feature-switch-using-spring-boot/-/blob/main/UserAccess/src/main/resources/application.yml).

Table 1 lists all the users along with their unique user ID, while Table 2 lists the features with their feature IDs. Table 3 has two columns which are both foreign keys, **user_feature.user_id** which references **all_user.id** in Table 1, and **user_feature.feature_id** which references **feature.id** in Table 2. This means that new entries in Table 3 must cross-reference records in Tables 1 and 2, in order to preserve referential integrity of the database.

**Table 1: all_user**
| id (PK)                 | email                                                                        |
|-------------------------|------------------------------------------------------------------------------|
| Randomly-generated UUID | alice@gmail.com<br>ahmad@hotmail.com<br>lee@yahoo.com<br>ron@topglove.com.my |

**Table 2: feature**
| id (PK)    | feature_name            |
|------------|-------------------------|
| 1          | new_ui_redesign         |
| 2          | new_test_feature        |
| 3          | enable_admin_privileges |

**Table 3: user_feature**
| user_id (FK) | feature_id (FK) |
|--------------|-----------------|
| User ID 1    | 1<br>2<br>3     |
| User ID 2    | 1<br>2          |
| User ID 3    | 1               |


## Prerequisites
The present application assumes the following:
- A PostgreSQL instance is set up and running on the local machine.
- The appropriate port, username and password for the database instance to be entered in [application.yml](https://gitlab.com/secant-senpai/ml-feature-switch-using-spring-boot/-/blob/main/UserAccess/src/main/resources/application.yml).
- An existing database named 'userfeaturedatabase' is available for read-and-write, preferably without any tables within. Though, the database name can be set via [application.yml](https://gitlab.com/secant-senpai/ml-feature-switch-using-spring-boot/-/blob/main/UserAccess/src/main/resources/application.yml).

## Building the application
To compile the code from source, execute the following in Command Prompt or Terminal:

``` shell
mvn compile
```

To package the code, run the following instead:

``` shell
mvn package
```

To execute the packaged application, run:

``` shell
java -jar UserAccess/target/UserAccess<VERSION>.jar
```

## Testing the API
You can test the API by using Postman. Postman is a HTTP client which is used in API testing. It tests HTTP requests by utilizing a graphical user interface through which one obtains different types of responses that need to be subsequently validated. Postman can be downloaded [here](https://www.postman.com/downloads/).

Below is a GIF showing a test drive of the API using Postman, where one fetches the status of the user's access level and then updates that record in an existing PostgreSQL database.

![PostmanDemo](https://gitlab.com/secant-senpai/ml-feature-switch-using-spring-boot/-/raw/main/demo/PostmanDemo.gif)
